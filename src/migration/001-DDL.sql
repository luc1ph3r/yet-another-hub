START TRANSACTION;

CREATE TABLE IF NOT EXISTS Operation (
  id          INT         NOT NULL PRIMARY KEY,
  type        INT         NOT NULL,
  description VARCHAR(45)     NULL
);

CREATE TABLE IF NOT EXISTS Users (
  id       INT          NOT NULL PRIMARY KEY,
  username VARCHAR(45)  NOT NULL,
  email    VARCHAR(45)  NOT NULL,
  password VARCHAR(45)  NOT NULL,
  created  TIMESTAMP(1) NOT NULL
);

CREATE TABLE IF NOT EXISTS Repository (
  id        INT          NOT NULL PRIMARY KEY,
  name      VARCHAR(45)  NOT NULL,
  author_id INT          NOT NULL,
  created   TIMESTAMP(1) NOT NULL,
  CONSTRAINT author_id
    FOREIGN KEY (author_id)
    REFERENCES Users (id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Branch (
  id               INT          NOT NULL PRIMARY KEY,
  name             VARCHAR(45)  NOT NULL,
  repository_id    INT          NOT NULL,
  author_id        INT          NOT NULL,
  parent_branch_id INT              NULL,
  created          TIMESTAMP(1) NOT NULL,
  last_edit        TIMESTAMP(1)     NULL,
  CONSTRAINT parent_branch
    FOREIGN KEY (parent_branch_id)
    REFERENCES Branch (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT rep_id
    FOREIGN KEY (repository_id)
    REFERENCES Repository (id)
    ON DELETE CASCADE,
  CONSTRAINT author_id
    FOREIGN KEY (author_id)
    REFERENCES Users (id)
    ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS privileges (
  user_id       INT        NOT NULL,
  repository_id INT        NOT NULL,
  branch_id     INT            NULL,
  operation_id  INT        NOT NULL,
  CONSTRAINT user_id
    FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON DELETE CASCADE,
  CONSTRAINT repository_id
    FOREIGN KEY (repository_id)
    REFERENCES repository (id)
    ON DELETE CASCADE,
  CONSTRAINT operation_id
    FOREIGN KEY (operation_id)
    REFERENCES operation (id)
    ON DELETE CASCADE
    ON UPDATE RESTRICT,
  CONSTRAINT branch_id
    FOREIGN KEY (branch_id)
    REFERENCES Branch (id)
    ON DELETE CASCADE,
  CONSTRAINT primary_key
  PRIMARY KEY (
    user_id,
    repository_id,
    branch_id,
    operation_id
  )
);

CREATE TABLE IF NOT EXISTS commits (
  id             INT          NOT NULL PRIMARY KEY,
  description    VARCHAR(45)      NULL,
  author_id      INT          NOT NULL,
  repository_id  INT          NOT NULL,
  from_branch_id INT          NOT NULL,
  to_branch_id   INT          NOT NULL,
  created        TIMESTAMP(1) NOT NULL,
  CONSTRAINT author_id
    FOREIGN KEY (author_id)
    REFERENCES Users (id)
    ON DELETE NO ACTION,
  CONSTRAINT repository_id
    FOREIGN KEY (repository_id)
    REFERENCES Repository (id)
    ON DELETE CASCADE,
  CONSTRAINT from_branch
    FOREIGN KEY (from_branch_Id)
    REFERENCES Branch (id)
    ON DELETE NO ACTION,
  CONSTRAINT to_branch
    FOREIGN KEY (to_branch_id)
    REFERENCES Branch (id)
    ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS Codebase (
  id            INT          NOT NULL PRIMARY KEY,
  version       INT          NOT NULL,
  repository_id INT          NOT NULL,
  branch_id     INT          NOT NULL,
  next_id       INT              NULL,
  prev_id       INT              NULL,
  created       TIMESTAMP(1) NOT NULL,
  last_edit     TIMESTAMP(1)     NULL,
  CONSTRAINT repository_id
    FOREIGN KEY (repository_id)
    REFERENCES Repository (id)
    ON DELETE CASCADE,
  CONSTRAINT branch_id
    FOREIGN KEY (branch_id)
    REFERENCES Branch (id)
    ON DELETE CASCADE,
  CONSTRAINT next
    FOREIGN KEY (next_id)
    REFERENCES Codebase (id)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT prev
    FOREIGN KEY (prev_id)
    REFERENCES Codebase (id)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS Folder (
  id          INT         NOT NULL PRIMARY KEY,
  codebase_id INT         NOT NULL,
  name        VARCHAR(45) NOT NULL,
  prev        INT             NULL,
  CONSTRAINT codebase_id
    FOREIGN KEY (codebase_id)
    REFERENCES Codebase (id)
    ON DELETE CASCADE,
  CONSTRAINT prev
    FOREIGN KEY (prev)
    REFERENCES Folder (id)
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS File (
  id            INT            NOT NULL PRIMARY KEY,
  repository_id INT            NOT NULL,
  codebase_id   INT            NOT NULL,
  name          VARCHAR(45)    NOT NULL,
  content       VARCHAR(30000)     NULL,
  created       VARCHAR(45)    NOT NULL,
  last_edit     TIMESTAMP(1)       NULL,
  folder_id     INT                NULL,
  CONSTRAINT repository_id
    FOREIGN KEY (repository_id)
    REFERENCES Repository (id)
    ON DELETE CASCADE,
  CONSTRAINT codebase_id
    FOREIGN KEY (codebase_id)
    REFERENCES Codebase (id)
    ON DELETE CASCADE,
  CONSTRAINT folder_id
    FOREIGN KEY (folder_id)
    REFERENCES Folder (id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Diff (
  id          INT            NOT NULL PRIMARY KEY,
  commit_id   INT            NOT NULL,
  old_file_id INT                NULL,
  new_file_id INT            NOT NULL,
  content     VARCHAR(40000) NOT NULL,
  created     TIMESTAMP(1)   NOT NULL,
  CONSTRAINT commit_id
    FOREIGN KEY (commit_id)
    REFERENCES Commits (id)
    ON DELETE CASCADE,
  CONSTRAINT old_file_id
    FOREIGN KEY (old_file_id)
    REFERENCES File (id)
    ON DELETE NO ACTION,
  CONSTRAINT new_file_id
    FOREIGN KEY (new_file_id)
    REFERENCES File (id)
    ON DELETE CASCADE
);

COMMIT;